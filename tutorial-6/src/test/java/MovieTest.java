import static org.junit.Assert.assertEquals;

import java.util.Objects;

import org.junit.Before;
import org.junit.Test;

public class MovieTest {
    Movie movie;

    @Before
    public void setUp() {
        this.movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
    }

    @Test
    public void getTitle() {
        assertEquals("Who Killed Captain Alex?", movie.getTitle());
    }

    @Test
    public void setTitle() {
        movie.setTitle("Bad Black");

        assertEquals("Bad Black", movie.getTitle());
    }

    @Test
    public void getPriceCode() {
        assertEquals(Movie.REGULAR, movie.getPriceCode());
    }

    @Test
    public void setPriceCode() {
        movie.setPriceCode(Movie.CHILDREN);

        assertEquals(Movie.CHILDREN, movie.getPriceCode());
    }

    @Test
    public void equalsNull() {
        assertEquals(false, movie.equals(null));
    }

    @Test
    public void testHashCode() {
        assertEquals(Objects.hash(movie.getTitle(), movie.getPriceCode()), movie.hashCode());
    }

    @Test
    public void testSomething() {
        Movie hehe = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        assertEquals(true, movie.equals(hehe));
    }
}
