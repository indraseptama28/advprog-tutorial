import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class Customer {

    private String name;
    private List<Rental> rentals = new ArrayList<>();

    public Customer(String name) {
        this.name = name;
    }

    public void addRental(Rental arg) {
        rentals.add(arg);
    }

    public String getName() {
        return name;
    }

    public String statement() {
        Iterator<Rental> iterator = rentals.iterator();
        String result = "Rental Record for " + getName() + "\n";

        while (iterator.hasNext()) {
            Rental each = iterator.next();
            double thisAmount = each.getThisAmount();

            // Show figures for this rental
            result += "\t" + each.getMovie().getTitle() + "\t"
                    + String.valueOf(thisAmount) + "\n";
        }

        // Add footer lines
        result += "Amount owed is " + String.valueOf(getTotalAmount()) + "\n";
        result += "You earned " + String.valueOf(getTotalFrequentRenterPoints())
                + " frequent renter points";

        return result;
    }

    private int getTotalFrequentRenterPoints() {
        int frequentRenterPoints = 0;
        Iterator<Rental> iterator = rentals.iterator();
        while (iterator.hasNext()) {
            Rental each = iterator.next();
            frequentRenterPoints++;
            frequentRenterPoints = each.getFrequentRenterPoints(frequentRenterPoints);
        }
        return frequentRenterPoints;
    }

    private double getTotalAmount() {
        Iterator<Rental> iterator = rentals.iterator();
        double totalAmount = 0;
        while (iterator.hasNext()) {
            Rental each = iterator.next();
            for (Rental rent : rentals) {
                totalAmount += rent.getThisAmount();
            }
        }
        return totalAmount;
    }
}