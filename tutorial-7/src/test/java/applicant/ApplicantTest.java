package applicant;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.function.Predicate;

import org.junit.Before;
import org.junit.Test;

// Increase code coverage in Applicant class
// by creating unit test(s)!
public class ApplicantTest {

    private Applicant applicant;

    @Before
    public void setUp() {
        applicant = new Applicant();
    }

    @Test
    public void isCredible() {
        assertTrue(applicant.isCredible());
    }

    @Test
    public void getCreditScore() {
        assertEquals(700, applicant.getCreditScore());
    }

    @Test
    public void getEmploymentYears() {
        assertEquals(10, applicant.getEmploymentYears());
    }

    @Test
    public void hasCriminalRecord() {
        assertTrue(applicant.hasCriminalRecord());
    }

    @Test
    public void evaluateCreditScore() {
        Predicate<Applicant> creditCheck = anApplicant -> anApplicant.getCreditScore() > 600;
        assertTrue(Applicant.evaluate(applicant, creditCheck));
    }

    @Test
    public void evaluateEmploymentYear() {
        Predicate<Applicant> employmentYearCheck = anApplicant ->
                anApplicant.getEmploymentYears() > 0;
        assertTrue(Applicant.evaluate(applicant, employmentYearCheck));

    }

    @Test
    public void evaluateCriminalRecord() {
        Predicate<Applicant> crimeCheck = anApplicant -> !anApplicant.hasCriminalRecord();
        assertFalse(Applicant.evaluate(applicant, crimeCheck));
    }

    @Test
    public void printEvalTest() {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        System.setOut(new PrintStream(baos));

    }
}
