package hello;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class GreetingController {

    @GetMapping("/greeting")
    public String greeting(@RequestParam(name = "name", required = true)
                                   String name, Model model) {
        model.addAttribute("name", name);
        if (name == null || name.equals(" ") || name.equals("")) {
            model.addAttribute("title", "This is my CV");
        } else {
            model.addAttribute("title", "Hello, " + name
                    + "! I hope you are interested to hire me");

        }

        return "greeting";
    }

}
