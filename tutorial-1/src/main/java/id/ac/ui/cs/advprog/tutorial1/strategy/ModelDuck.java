package id.ac.ui.cs.advprog.tutorial1.strategy;

public class ModelDuck extends Duck {
	public ModelDuck(){
		this.setFlyBehavior(new FlyNoWay());
		this.setQuackBehavior(new Squeak());
		this.setSwimBehavior(new Swim());
	}

	@Override
	public void display() {
		// TODO Auto-generated method stub
		System.out.println("I'm model duck");
	}

}
