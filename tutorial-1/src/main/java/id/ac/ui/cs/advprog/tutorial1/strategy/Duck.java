package id.ac.ui.cs.advprog.tutorial1.strategy;
//Indra Septama
public abstract class Duck {

    private FlyBehavior flyBehavior;
    private QuackBehavior quackBehavior;
    private SwimBehavior swimBehavior;
    
    public void performFly() {
        flyBehavior.fly();
    }

    public void performQuack() {
        quackBehavior.quack();
    }

    public void swim(){
    	swimBehavior.swim();
    }
    // TODO Complete me!
    
    public void setFlyBehavior(FlyBehavior fb){
    	flyBehavior = fb;
    }
    
    public void setQuackBehavior(QuackBehavior qb){
    	quackBehavior = qb;
    }
    
    public void setSwimBehavior(SwimBehavior sw){
    	this.swimBehavior = sw;
    }
    
    public abstract void display();
}
